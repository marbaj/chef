directory '/var/www/html' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end


file '/var/www/html/index.html' do
    owner "root"  
    group "root"  
    mode "0755"  
    action :create
end

file '/var/www/html/index.html' do
    content '<html>This is a placeholder for the home page.</html>'
end
